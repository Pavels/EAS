from django.contrib import admin

# Register your models here.
from .models import Registro,Perfil,Ausentismo,Nomina,Convocatoria,Resultados

#Register your models here.
@admin.register(Registro)
class RegistroAdmin(admin.ModelAdmin):
    list_display = ('nombres','apellidos','profesion','exp','estado','fecha_reg','fecha_modif',)
    list_filter = ('estado','fecha_modif','fecha_hdv')
    fieldsets = (
        ('Datos personales', {
            'fields': (('nombres', 'apellidos','fecha_nacimiento'), ('tipo_doc','num_doc'))
        }),
        ('Soportes', {
            'fields': (('hdv','fecha_hdv'), 'foto_perfil')
        }),
        ('Datos de contacto', {
            'fields': ('telefono', 'correo_personal')
        }),
        ('Perfil profesional', {
            'fields': ('profesion', 'exp')
        }),
        ('Estado en nómina', {
            'fields': ('estado',)
        }),
        #('Datos de nómina', {
        #    'fields': (('',))
        #}),
    )
@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ('cargo','tipo_edu','exp','descripcion',)

@admin.register(Ausentismo)
class AusentismoAdmin(admin.ModelAdmin):
    list_display = ('trabajador','tipo_lic','fecha_ini','fecha_fin')
    list_filter = ('tipo_lic','fecha_ini','fecha_fin')

@admin.register(Nomina)
class NominaAdmin(admin.ModelAdmin):
    list_display = ('trabajador','cargo','sede','tipo_estado','fecha_modif')
    list_filter = ('cargo','sede','tipo_estado')
@admin.register(Convocatoria)
class ConvocatoriaAdmin(admin.ModelAdmin):
    pass
@admin.register(Resultados)
class ResultadosAdmin(admin.ModelAdmin):
    pass
