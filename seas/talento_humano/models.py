from django.db import models
from sedes.models import Sede
from correos.models import Correo

# Create your models here.
class Registro(models.Model):
    """Este objeto representa a todos las personas que están registradas en la base de datos, estén o nó trabajando"""
    nombres= models.CharField('Nombres',max_length=100)
    apellidos = models.CharField('Apellidos',max_length=100)
    TIPOS_DE_DOCUMENTOS = (
        ('cc', 'Cédula de ciudadanía'),
        ('ce', 'Cédula de extranjería'),
        ('pp', 'Pasaporte'),
    )
    tipo_doc = models.CharField(max_length=2, choices=TIPOS_DE_DOCUMENTOS, default='cc', verbose_name='Tipo de Documento',help_text='Defina el tipo de documento')
    num_doc= models.CharField(max_length=13,verbose_name='Número de identificación',help_text='Ingresar el número de documento sin el punto decimal')
    fecha_nacimiento = models.DateField(verbose_name='Fecha de nacimiento',null=True, blank=True)
    hdv = models.FileField(upload_to='talento_humano/hdv',help_text='Adjuntar la hoja de vida',null=True, blank=True)
    fecha_hdv = models.DateField(verbose_name='Fecha de carga de hdv',help_text='Fecha en que se cargó al sistema la Hdv',null=True, blank=True)
    foto_perfil = models.ImageField(upload_to='talento_humano/fotos_de_perfiles',null=True,blank=True,verbose_name='Foto de perfil',help_text='Tamaño 170x150px Fondo=Blanco')
    telefono = models.CharField(verbose_name='Número telefónico',max_length=15,unique=True)
    correo_personal= models.EmailField(verbose_name='Correo electrónico',unique=True)
    profesion = models.CharField(blank=True, max_length=100,verbose_name='Profesión')
    TIPOS_DE_EXPERIENCIA = (
        ('a','Pasante'),
        ('b','1 años'),
        ('c','3 años'),
        ('d','5 años'),
        ('e','Más de 5 años'),
    )
    exp = models.CharField(max_length=1, choices=TIPOS_DE_EXPERIENCIA, blank=True, default='b', verbose_name='Experiencia',help_text='Defina los años de experiencia')
    fecha_reg=models.DateField('Registrado el dia',auto_now_add=True,help_text='Indica la fecha en que se creo el registro',blank=True, null=True)
    fecha_modif=models.DateField('Última Modificación del registro',auto_now=True,help_text='Indica la última modificación del registro',blank=True, null=True)
    estado = models.BooleanField(default=False,verbose_name='Estado',help_text='Define si es un trabajador activo')
    # AGREGAR UN VER EN SITIO PARA QUE PUEDA VERSE UN RESUMEN DEL PERFIL DE ESTA PERSONA, SEDE ASOCIADA, HOJA DE VIDA, FOTO, CORREO, ETC. LA EXPERIENCIA EN COASHOGARES SE DERIVA D ELA FECHA DE INGRESO.
    class Meta:
        unique_together = ('tipo_doc', 'num_doc',)
    def __str__(self):
        """
        Esta cadena de texto representa el objeto modelo.
        """
        return '%s, %s' % (self.nombres, self.apellidos)

class Perfil(models.Model):
    """Define el puesto de trabajo"""
    cargo= models.CharField(max_length=20,verbose_name='Cargo')
    descripcion = models.TextField(help_text='Descripción del Cargo',blank=True, null=True)
    TIPOS_DE_EXPERIENCIA = (
        ('a','Pasante'),
        ('b','1 años'),
        ('c','3 años'),
        ('d','5 años'),
        ('e','Más de 5 años'),
    )
    exp = models.CharField(max_length=1, choices=TIPOS_DE_EXPERIENCIA, blank=True, default='e', verbose_name='Experiencia requerida',help_text='Defina la experiencia del perfil')
    TIPOS_DE_EDUCACION = (
        ('tec', 'Técnico'),
        ('lic', 'Licenciado'),
        ('pos', 'Post-grado'),
    )
    tipo_edu = models.CharField(max_length=3, choices=TIPOS_DE_EDUCACION, blank=True, default='lic', verbose_name='Nivel de educación',help_text='Defina el grado de educación requerido')
    class Meta:
        verbose_name_plural='Perfiles'
    def __str__(self):
        """
        Esta cadena de texto representa el objeto modelo.
        """
        return '%s' %self.cargo
class Ausentismo(models.Model):
    """Define el estado de las inasistencias"""
    trabajador=models.ForeignKey(Registro, on_delete=models.SET_NULL, null=True)
    TIPOS_DE_LICENCIA = (
        ('m','Maternidad'),
        ('e','Reposo médico'),
        ('d','Muerte de familiar'),
        ('o','Otro'),
    )
    tipo_lic = models.CharField(max_length=1, choices=TIPOS_DE_LICENCIA, blank=True, default='e', verbose_name='Tipo de licencia',help_text='Defina el tipo de licencimiento o motivo de ausencia')
    descripcion = models.TextField(help_text='Descripción del caso',blank=True, null=True)
    fecha_ini = models.DateField(verbose_name='Fecha de salida',blank=True,null=True)
    fecha_fin = models.DateField(verbose_name='Fecha de retorno',blank=True,null=True)
    #Añadir soportes
    def __str__(self):
        """
        Esta cadena de texto representa el objeto modelo.
        """
        return '%s Licencia "%s"' %(self.trabajador,self.tipo_lic)
class Nomina(models.Model):
    """representa la base de datos de puestos de trabajo y trabajadores"""
    trabajador=models.ForeignKey(Registro,on_delete=models.SET_NULL,verbose_name='Trabajador asignado',blank=True,null=True)
    cargo=models.ForeignKey(Perfil,on_delete=models.SET_NULL,verbose_name='Cargo asignado',blank=True,null=True)
    sede=models.ForeignKey(Sede,on_delete=models.SET_NULL,verbose_name='Sede asignada',blank=True,null=True)
    correo_asig=models.ForeignKey(Correo,on_delete=models.SET_NULL, null=True,verbose_name='Correo asignado')
    TIPOS_DE_ESTADO = (
        ('v','Vacante'),
        ('i','Indefinido'),
        ('a','Activo'),
    )
    tipo_estado = models.CharField(max_length=1, choices=TIPOS_DE_ESTADO, blank=True, default='i', verbose_name='Estádo de nómina',help_text='Define si el cargo esta ocupado, vacante o en un estado indefinido')
    fecha_modif=models.DateField(verbose_name='Ultima Modificación',auto_now=True,help_text='Indica la última modificación',blank=True, null=True)
    class Meta:
        verbose_name_plural = ('Nomina')
    def __str__(self):
        return '%s %s' %(self.trabajador,self.cargo)
class Convocatoria(models.Model):
    cargo=models.ForeignKey(Perfil,on_delete=models.SET_NULL,verbose_name='Cargo de vacante',blank=True,null=True)
    lugar=models.ForeignKey(Sede,on_delete=models.SET_NULL,verbose_name='Ubicación de vacante',blank=True,null=True)
    descripcion=models.TextField(help_text='Descripción del Cargo',blank=True, null=True)
    pdf_hdv = models.FileField(upload_to='talento_humano/convocatorias',help_text='Adjuntar pdf de vacante',null=True, blank=True)
    fecha_ini=models.DateField('Inicio',help_text='Inicio de la convocatoria',blank=True, null=True)
    fecha_fin=models.DateField('Fin',help_text='Fin de la convocatoria',blank=True, null=True)
    estado_convocatoria = models.BooleanField(default=False,verbose_name='Estado',help_text='Finaliza eliminando la convocatoria de www.coashoares.com')

class Resultados(models.Model):
    conv=models.ForeignKey(Convocatoria,on_delete=models.SET_NULL,verbose_name='Nombre de convocatoria',blank=True,null=True)
    num_fb = models.IntegerField(blank=True, null=True,verbose_name='Vistas en la página de Facebook')
    num_des = models.IntegerField(blank=True, null=True,verbose_name='Descargas del pdf ',help_text='Descargas del pdf  en www.coashogare.com')
    num_correo = models.IntegerField(blank=True, null=True,verbose_name='Numero de Registros ',help_text='Correos recibidos a oferta.laboral@coashogare.com')
    #CREAR UN ATRIBUTO DE REGISTROS QUE SEA MANYTOMANYFIELDS para agregar a todos los perfiles obtenidos
    #Asociar una clase Registros_convocatorias donde se almacenen todos los registros obtenidos por convocatoria
    class Meta:
        verbose_name_plural = ('Resultados de convocatorias')
