from django.db import models
#from talento_humano.models import Registro

# Create your models here.
class Sede(models.Model):
    """Este objeto representa a todos las sedes de operacion o grupos de atención"""
    nombre= models.CharField('Nombre de SD o GA',max_length=100,help_text='Indique el nombre de la sede(SD) o grupo de atención(GA)')
    direccion = models.CharField('Dirección',max_length=100)
    telefono = models.CharField(verbose_name='Número telefónico',max_length=15)
    #trabajadores = models.ManyToManyField(Registro)
    def __str__(self):
        """
        Esta cadena de texto representa el objeto modelo.
        """
        return '%s' % (self.nombre)
