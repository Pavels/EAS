from django.contrib import admin
from .models import Sede
# Register your models here.

# Register your models here.
@admin.register(Sede)
class SedeAdmin(admin.ModelAdmin):
    list_display = ('nombre','direccion', 'telefono',)
