from django.contrib import admin
from .models import Correo, Cregistro,Cespecial
# Register your models here.
@admin.register(Correo)
class CorreoAdmin(admin.ModelAdmin):
    list_display = ('correo_corp','pw','estado')
@admin.register(Cregistro)
class  CregistroAdmin(admin.ModelAdmin):
    list_display = ('correo','sede','asig','fecha_asig','re_asig','fecha_re_asig','c_respaldo')
    list_filter = ('sede','fecha_asig')
@admin.register(Cespecial)
class CregistroAdmin(admin.ModelAdmin):
    """docstring for CregistroAdmin."""
    list_display=('correo_esp','uso')
